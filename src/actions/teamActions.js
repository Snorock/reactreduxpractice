import { FETCH_TEAMS, FETCH_USERS } from "./types";

export function fetchTeams(){
    return function(dispatch){
        fetch('https://cgjresszgg.execute-api.eu-west-1.amazonaws.com/teams/')
        .then(res => res.json())
        .then(teams => dispatch({
            type: FETCH_TEAMS,
            infos: teams
        }))
        .catch(err => console.error(err));
    }
}

export function fetchUsers(){
    return function(dispatch){
        fetch('https://cgjresszgg.execute-api.eu-west-1.amazonaws.com/users/')
        .then(res => res.json())
        .then(users => dispatch({
            type: FETCH_USERS,
            infos: users
        }))
        .catch(err => console.error(err));
    }
}