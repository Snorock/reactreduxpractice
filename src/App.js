import React, { Component } from 'react';
import { Provider } from 'react-redux';
import './App.css';
import store from "./store";

import TeamsList from './components/TeamsList';

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <div className="App" data-testid='App'>
          <TeamsList />
        </div>
      </Provider>
    )
  }
}

export default App;