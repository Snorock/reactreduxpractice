import React, { Component } from 'react'
import './TeamDetails.css';

class TeamDetails extends Component {
    constructor(props){
        super(props);
        this.state = {
            filterBy: '',
        };

        this.onChangeFilter = this.onChangeFilter.bind(this)
    }

    onChangeFilter(e){
        this.setState({ filterBy: e.target.value });
    }

    render() {
        const usersListDisplay = this.props.users.filter(curr => {
            return this.state.filterBy.length<3 || curr.name.toLowerCase().includes(this.state.filterBy.toLowerCase())
        }).map(curr => (
            <div 
                className={`box link-like has-background-info-light`}
                key={curr.id}
                data-testid={'listItem' + curr.id}
            >{curr.name}</div>
        ));
        return (
            <div className="container has-text-centered">
                <div className="title" data-testid='teamlabel'>{this.props.team.name}</div>
                <div className="columns">
                    <div className="column is-4"><button data-testid='clearSelectionButton' onClick={this.props.clearSelection} className='button is-outlined is-rounded'>Clear</button></div>
                    <div className="column is-4 title is-large subtitle">Members:</div>
                    <div className="column is-4">
                        Filter By: &nbsp;
                        <input 
                            type="text" 
                            name="filter" 
                            data-testid='filterByField'
                            onChange={this.onChangeFilter}
                            value={this.state.filterBy}
                        ></input>
                    </div>
                </div> 
                <div>
                    {usersListDisplay}
                </div>
            </div>
        )
    }
}

export default TeamDetails;