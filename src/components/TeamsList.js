import React, { Component } from 'react';
import { connect } from "react-redux";
import { fetchTeams, fetchUsers } from "../actions/teamActions";
import './TeamsList.css';

import TeamDetails from "./TeamDetails";

class TeamsList extends Component {
    constructor(props){
        super(props);
        this.state = {
            filterBy: '',
            selectedTeam: null,
            selectedUsers: [],
        };

        this.onChangeFilter = this.onChangeFilter.bind(this)
        this.onSelect = this.onSelect.bind(this);
        this.clearSelection = this.clearSelection.bind(this);
    }

    componentWillMount(){
        this.props.fetchTeams();
        this.props.fetchUsers();
    }

    onChangeFilter(e){
        this.setState({ filterBy: e.target.value });
    }

    onSelect(e){
        const teamId = e.currentTarget.dataset.id;
        const team = this.props.teams.find( curr => curr.id === teamId );
        const teamUsers = this.props.users.filter( curr => curr.teamId.includes(teamId))
        this.setState({ selectedUsers: teamUsers });
        this.setState({ selectedTeam: team });
    }

    clearSelection(e){
        this.setState({ selectedUsers: [] });
        this.setState({ selectedTeam: null });
    }

    render() {
        const teamsListDisplay = this.props.teams.filter(curr => {
            return this.state.filterBy.length<3 || curr.name.toLowerCase().includes(this.state.filterBy.toLowerCase())
        }).map(curr => (
            <div 
                className={`box link-like ${this.state.selectedTeam?.id === curr.id ? 'has-background-success-light' : 'has-background-info-light'}`}
                key={curr.id}
                data-id={curr.id}
                onClick={this.onSelect}
            >{curr.name}</div>
        ));
        return (
            <div className="container">
                <br />
                <div className="columns">
                    <div className="column is-offset-2 is-4 title is-large">Teams:</div>
                    <div className="column is-offset1 is-5">
                        Filter By: &nbsp;
                        <input 
                            type="text" 
                            name="filter" 
                            onChange={this.onChangeFilter}
                            value={this.state.filterBy}
                        ></input>
                    </div>
                </div>    
                <div className="menu has-text-centered list-main box is-rounded">
                    {teamsListDisplay}
                </div>     
                {this.state.selectedTeam ?
                    <TeamDetails clearSelection={this.clearSelection} team={this.state.selectedTeam} users={this.state.selectedUsers} />
                : ""}       
            </div>
        )
    }
}

const stateToProps = state => ({
    teams: state.teams.teams,
    users: state.teams.users
});

export default connect(stateToProps, { fetchTeams, fetchUsers })(TeamsList);