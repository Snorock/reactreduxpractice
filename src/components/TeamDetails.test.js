import { render, screen, fireEvent } from '@testing-library/react';
import TeamDetails from './TeamDetails';

const propTeam = { id:'test1', name:'test name' };
const propUsers = [{ id:'test1', name:'test name' }];

test('renders static label', () => {
    render(<TeamDetails team={propTeam} users={propUsers} />);
    const linkElement = screen.getByText(/Members/i);
    expect(linkElement).toBeInTheDocument();
  });

test('renders list element', () => {
    render(<TeamDetails team={propTeam} users={propUsers} />);
    const linkElement = screen.getByTestId('listItemtest1');
    expect(linkElement).toHaveTextContent('test name');
  });

describe("filterBy field", ()=> {
    test('does not filter with short input', () => {
        render(<TeamDetails team={propTeam} users={propUsers} />);
        const inputField = screen.getByTestId('filterByField');
        fireEvent.change(inputField, {target: {value:'xy'} })
        const linkElement = screen.getByTestId('listItemtest1');
        expect(linkElement).toHaveTextContent('test name');
      });
    test('include correctly', () => {
        render(<TeamDetails team={propTeam} users={propUsers} />);
        const inputField = screen.getByTestId('filterByField');
        fireEvent.change(inputField, {target: {value:'test'} })
        const linkElement = screen.getByTestId('listItemtest1');
        expect(linkElement).toHaveTextContent('test name');
      });
    
    test('exclude correctly', () => {
        render(<TeamDetails team={propTeam} users={propUsers} />);
        const inputField = screen.getByTestId('filterByField');
        fireEvent.change(inputField, {target: {value:'xyz'} })
        const linkElement = screen.queryAllByTestId('listItemtest1');
        expect(linkElement).toStrictEqual([]);
      });

    });
