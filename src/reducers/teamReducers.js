import { FETCH_TEAMS, FETCH_USERS } from "../actions/types";

const initialState = {
    teams: [],
    users: []
}

export default function(state=initialState, action){
    switch(action.type){
        case FETCH_TEAMS:
            return {
                ...state,
                teams: action.infos
            }
        case FETCH_USERS:
            return {
                ...state,
                users: action.infos
            }
        default:
            return state;
    }
}