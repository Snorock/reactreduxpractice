import { combineReducers } from "redux";
import teamReducers from './teamReducers';

export default combineReducers({
    teams: teamReducers
});